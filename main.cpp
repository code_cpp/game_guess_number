#include <iostream>

using namespace std;

int main()
{
    /*
    THE RULES
    1. Player 1 selects a random number
    2. Player 2 needs to guess the number
    3. The game will tell Player 2 whether the guess is too low or too high
    4. The game will count the number of attempts Player 2 made
    */
    int number = 0;
    int attempts = 0;
    int guess = 0;
    cout << "Player1, please, enter the number:" << endl;
    cin >> number;
    while (guess != number) {
        cout << "Player2, please, guess the number:" << endl;
        cin >> guess;
        if(guess > number) {
            cout << "Number is less" << endl;
        } else if (guess < number) {
            cout << "Number is more" << endl;
        } else {
            cout << "You are win!" << endl;
        }
        attempts++;
    }
    cout << "Player2 made " << attempts << " attempts" << endl;


    return 0;
}
